<?php

namespace Tests;

use LendInvest\Enhance\DateTime;
use LendInvest\Entity\Investor;
use LendInvest\Entity\Loan;
use LendInvest\Entity\Tranche;
use LendInvest\Entity\Transaction;
use LendInvest\Service\InvestorService;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestorServiceTransferMoneyTest
 * @package Tests
 */
class InvestorServiceTransferMoneyTest extends TestCase
{

    /**
     * @var InvestorService
     */
    private $investorService;

    /**
     * @var Loan
     */
    private $loan;

    /**
     * @var Investor
     */
    private $investor1;

    /**
     * @var Investor
     */
    private $investor2;

    /**
     * @var Investor
     */
    private $investor3;

    /**
     * @var Investor
     */
    private $investor4;

    public function setUp()
    {
        $this->investorService = new InvestorService();

        DateTime::shiftTime(null);

        $tranche1 = new Tranche();
        $tranche1->setCode('A');
        $tranche1->setMaxAmount(100000);
        $tranche1->setMonthlyInterestPercentage(3);

        $tranche2 = new Tranche();
        $tranche2->setCode('B');
        $tranche2->setMaxAmount(100000);
        $tranche2->setMonthlyInterestPercentage(6);

        $loanDateStart = new DateTime('2015-10-01');
        $loanDateEnd   = new DateTime('2015-11-15');
        $this->loan = new Loan($loanDateStart, $loanDateEnd);
        $this->loan->addTranche($tranche1);
        $this->loan->addTranche($tranche2);

        $this->investor1 = new Investor();
        $this->investor1->setUsername('Investor 1');
        // put 1000 pounds to investor's wallet
        $this->investor1->getWallet()->addTransaction(new Transaction($this->investor1, 100000, Transaction::STATUS_DONE));

        $this->investor2 = new Investor();
        $this->investor2->setUsername('Investor 2');
        // put 1000 pounds to investor's wallet
        $this->investor2->getWallet()->addTransaction(new Transaction($this->investor2, 100000, Transaction::STATUS_DONE));

        $this->investor3 = new Investor();
        $this->investor3->setUsername('Investor 3');
        // put 1000 pounds to investor's wallet
        $this->investor3->getWallet()->addTransaction(new Transaction($this->investor2, 100000, Transaction::STATUS_DONE));

        $this->investor4 = new Investor();
        $this->investor4->setUsername('Investor 4');
        // put 1000 pounds to investor's wallet
        $this->investor4->getWallet()->addTransaction(new Transaction($this->investor2, 100000, Transaction::STATUS_DONE));

        DateTime::shiftTime(strtotime('2015-10-03'));
        $this->investorService->doInvestment($this->investor1, $this->loan, 'A', 100000);
        DateTime::shiftTime(strtotime('2015-10-10'));
        $this->investorService->doInvestment($this->investor3, $this->loan, 'B', 50000);
    }

    public function testTransferInterest()
    {
        $investor1MoneyBeforeTransfer = $this->investor1->getWallet()->getBalance();
        $investor2MoneyBeforeTransfer = $this->investor2->getWallet()->getBalance();
        $investor3MoneyBeforeTransfer = $this->investor3->getWallet()->getBalance();
        $investor4MoneyBeforeTransfer = $this->investor4->getWallet()->getBalance();

        DateTime::shiftTime(null);
        $dateStart = new DateTime('2015-10-01');
        $dateEnd   = new DateTime('2015-11-01');
        DateTime::shiftTime(strtotime('2015-11-01'));

        $this->investorService->transferMonthlyInterest($this->loan, $dateStart, $dateEnd);

        $investor1MoneyAfterTransfer = $this->investor1->getWallet()->getBalance();
        $investor2MoneyAfterTransfer = $this->investor2->getWallet()->getBalance();
        $investor3MoneyAfterTransfer = $this->investor3->getWallet()->getBalance();
        $investor4MoneyAfterTransfer = $this->investor4->getWallet()->getBalance();

        $this->assertEquals(2806, $investor1MoneyAfterTransfer - $investor1MoneyBeforeTransfer);
        $this->assertEquals(0, $investor2MoneyAfterTransfer - $investor2MoneyBeforeTransfer);
        $this->assertEquals(2129, $investor3MoneyAfterTransfer - $investor3MoneyBeforeTransfer);
        $this->assertEquals(0, $investor4MoneyAfterTransfer - $investor4MoneyBeforeTransfer);
    }

}
