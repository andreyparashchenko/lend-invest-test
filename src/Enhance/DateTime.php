<?php

namespace LendInvest\Enhance;

use DateTimeZone;

/**
 * Class DateTime
 * @package LendInvest\Enhance
 */
class DateTime extends \DateTime
{
    static protected $timestamp;

    public function __construct($time = 'now', DateTimeZone $timezone = null)
    {
        parent::__construct($time, $timezone);

        if (self::$timestamp > 0) {
            $this->setTimestamp(self::$timestamp);
        }
    }

    /**
     * @inheritdoc
     */
    public static function shiftTime($unixtimestamp)
    {
        self::$timestamp = $unixtimestamp;
    }
}