<?php

namespace LendInvest\Entity;

/**
 * Class Investor
 * @package LendInvest\Entity
 */
class Investor extends User
{
    /**
     * @param int $sumToInvest
     *
     * @return bool
     */
    public function hasSumToInvest(int $sumToInvest): bool
    {
        $userBalance = $this->getWallet()->getBalance();

        return $userBalance >= $sumToInvest;
    }
}