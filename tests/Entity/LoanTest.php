<?php

namespace Tests;

use LendInvest\Enhance\DateTime;
use LendInvest\Entity\Loan;
use LendInvest\Entity\Tranche;
use PHPUnit\Framework\TestCase;

/**
 * Class LoanTest
 * @package Tests
 */
class LoanTest extends TestCase
{
    /**
     * @var Loan
     */
    private $loan;

    public function setUp()
    {
        DateTime::shiftTime(null);

        $tranche1 = new Tranche();
        $tranche1->setCode('A');
        $tranche1->setMaxAmount(1000);
        $tranche1->setMonthlyInterestPercentage(3);

        $tranche2 = new Tranche();
        $tranche2->setCode('B');
        $tranche2->setMaxAmount(1000);
        $tranche2->setMonthlyInterestPercentage(6);

        $loanDateStart = new DateTime('2015-10-01');
        $loanDateEnd   = new DateTime('2015-11-15');
        $this->loan = new Loan($loanDateStart, $loanDateEnd);
        $this->loan->addTranche($tranche1);
        $this->loan->addTranche($tranche2);
    }

    /**
     * @param string $dateNow
     * @param bool   $expectedResult
     *
     * @dataProvider providerOpenForInvestment
     */
    public function testIsOpenedForInvestment(string $dateNow, bool $expectedResult)
    {
        DateTime::shiftTime(strtotime($dateNow));

        $this->assertEquals($expectedResult, $this->loan->isOpenedForInvestment());

        /** @var Tranche $tranche */
        $tranche = $this->loan->getTranches()->offsetGet(0);
        $tranche->setDone(1);

        // one tranche has been closed but loan still open for investment
        $this->assertEquals($expectedResult, $this->loan->isOpenedForInvestment());

        foreach ($this->loan->getTranches() as $tranche) {
            /** @var Tranche $tranche */
            $tranche->setDone(1);
        }

        // all tranches are closed - loan closed for investment
        $this->assertEquals(false, $this->loan->isOpenedForInvestment());
    }

    public function providerOpenForInvestment()
    {
        return [
            ['2015-09-10', true],
            ['2015-09-30', true],
            ['2015-10-01', true],
            ['2015-11-16', false],
            ['2015-11-30', false],
        ];
    }

    /**
     * @param string $key
     * @param bool   $expectedResult
     *
     * @dataProvider providerTranches
     */
    public function testGetTrancheByKey(string $key, bool $expectedResult)
    {
        $tranche = $this->loan->getTrancheByKey($key);

        $actualResult = $tranche ? $tranche->getCode() === $key : false;

        $this->assertEquals($expectedResult, $actualResult);
    }

    public function providerTranches()
    {
        return [
            ['A', true, ],
            ['B', true, ],
            ['C', false, ],
        ];
    }

}
