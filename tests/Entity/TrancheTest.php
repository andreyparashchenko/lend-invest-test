<?php

namespace Tests;

use LendInvest\Entity\Tranche;
use LendInvest\Entity\Transaction;
use LendInvest\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Class TrancheTest
 * @package Tests
 */
class TrancheTest extends TestCase
{

    public function testSumOfInvestment()
    {
        $user = new User();
        $transaction1 = new Transaction($user, 22, Transaction::STATUS_DONE);
        $transaction2 = new Transaction($user, 11, Transaction::STATUS_IN_PROGRESS);
        $transaction3 = new Transaction($user, 224, Transaction::STATUS_FAILED);
        $transaction4 = new Transaction($user, 999, Transaction::STATUS_DONE);

        $tranche = new Tranche();
        $tranche->addTransaction($transaction1);
        $tranche->addTransaction($transaction2);
        $tranche->addTransaction($transaction3);
        $tranche->addTransaction($transaction4);

        $this->assertEquals(1021, $tranche->getSumOfInvestment());
    }
}
