<?php

namespace LendInvest\Entity;

/**
 * Class User
 * @package LendInvest\Entity
 */
class User
{
    /**
     * @var string
     */
    private $username = '';

    /**
     * @var UserWallet
     */
    private $wallet;

    public function __construct()
    {
        $this->wallet = new UserWallet();
    }

    /**
     * @return UserWallet
     */
    public function getWallet(): UserWallet
    {
        return $this->wallet;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }
}