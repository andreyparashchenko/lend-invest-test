<?php

namespace LendInvest\Command;

/**
 * Interface CommandInterface
 * @package LendInvest\Command
 */
interface CommandInterface {

    public function execute();
}