<?php

namespace LendInvest;

use LendInvest\Enhance\DateTime;
use LendInvest\Entity\Investor;
use LendInvest\Entity\Loan;
use LendInvest\Entity\Tranche;
use LendInvest\Entity\Transaction;
use LendInvest\Exception\InvestmentNotDoneException;
use LendInvest\Exception\LoanClosedForInvestmentException;
use LendInvest\Exception\NotFoundException;
use LendInvest\Service\InvestorService;

include 'vendor/autoload.php';

$investor1 = new Investor();
$investor1->setUsername('Investor 1');
// put 1000 pounds to investor's wallet
$investor1->getWallet()->addTransaction(new Transaction($investor1, 1000, Transaction::STATUS_DONE));

$investor2 = new Investor();
$investor2->setUsername('Investor 2');
// put 1000 pounds to investor's wallet
$investor2->getWallet()->addTransaction(new Transaction($investor2, 1000, Transaction::STATUS_DONE));

$investor3 = new Investor();
$investor3->setUsername('Investor 3');
// put 1000 pounds to investor's wallet
$investor3->getWallet()->addTransaction(new Transaction($investor2, 1000, Transaction::STATUS_DONE));

$investor4 = new Investor();
$investor4->setUsername('Investor 4');
// put 1000 pounds to investor's wallet
$investor4->getWallet()->addTransaction(new Transaction($investor2, 1000, Transaction::STATUS_DONE));

$loanDateStart = new DateTime('2015-10-01');
$loanDateEnd   = new DateTime('2015-11-15');

$tranche1 = new Tranche();
$tranche1->setCode('A');
$tranche1->setMaxAmount(1000);
$tranche1->setMonthlyInterestPercentage(3);

$tranche2 = new Tranche();
$tranche2->setCode('B');
$tranche2->setMaxAmount(1000);
$tranche2->setMonthlyInterestPercentage(6);

$loan = new Loan($loanDateStart, $loanDateEnd);
$loan->addTranche($tranche1);
$loan->addTranche($tranche2);

// go back to 2015-10-03
DateTime::shiftTime(strtotime('2015-10-03'));
invest($investor1, $loan, 1000, 'A');

// go back to 2015-10-04
DateTime::shiftTime(strtotime('2015-10-04'));
invest($investor1, $loan, 1, 'A');

// go back to 2015-10-10
DateTime::shiftTime(strtotime('2015-10-10'));
invest($investor3, $loan, 500, 'B');

// go back to 2015-10-25
DateTime::shiftTime(strtotime('2015-10-25'));
invest($investor4, $loan, 1100, 'B');

// go back to 2015-11-01
DateTime::shiftTime(strtotime('2015-11-01'));
(new InvestorService())->transferMonthlyInterest($loan, new DateTime('2015-10-01'), new DateTime('2015-10-31'));
echo "\n Money on investor 1 wallet is: " . $investor1->getWallet()->getBalance();
echo "\n Money on investor 2 wallet is: " . $investor2->getWallet()->getBalance();
echo "\n Money on investor 3 wallet is: " . $investor3->getWallet()->getBalance();
echo "\n Money on investor 4 wallet is: " . $investor4->getWallet()->getBalance();

/**
 * @param Investor $investor
 * @param Loan     $loan
 * @param int      $sum
 * @param string   $trancheKey
 */
function invest(Investor $investor, Loan $loan, int $sum, string $trancheKey)
{
    $investorService = new InvestorService();

    try {

        $investorService->doInvestment($investor, $loan, $trancheKey, $sum);

        echo "\n" . $investor->getUsername() . " successfully invested to tranche {$trancheKey}";

        echo "\n Money on investor wallet is: " . $investor->getWallet()->getBalance();


    } catch (LoanClosedForInvestmentException $e) {
        echo "\n" . $investor->getUsername() . " can not invest as loan is closed";
    } catch (NotFoundException $e) {
        echo "\n {$investor->getUsername()} can not invest as tranche {$trancheKey} is not found";
    } catch (InvestmentNotDoneException $e) {
        echo "\n {$investor->getUsername()} can not invest to tranche {$trancheKey} as has no enough money or tranche {$trancheKey} is closed";
    }
}




