<?php

namespace Tests;

use LendInvest\Entity\Investor;
use LendInvest\Entity\Transaction;

/**
 * Class InvestorTest
 * @package Tests
 */
class InvestorTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @param int  $sumOnWallet
     * @param int  $sumToInvest
     * @param bool $expectedResult
     *
     * @dataProvider providerSum
     */
    public function testHasSumToInvest(int $sumOnWallet, int $sumToInvest, bool $expectedResult)
    {
        $investor = new Investor();
        $investor->getWallet()->addTransaction(new Transaction($investor, $sumOnWallet, Transaction::STATUS_DONE));

        $this->assertEquals($expectedResult, $investor->hasSumToInvest($sumToInvest));
    }

    public function providerSum()
    {
        return [
            [0, 1, false],
            [1, 0, true],
            [1, 1, true],
            [1, 2, false],
            [1999, 2000, false],
            [100000000000, 100000000000, true],
        ];
    }
}
