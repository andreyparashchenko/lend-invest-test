<?php

namespace LendInvest\Entity;

/**
 * Class UserWallet
 * @package LendInvest\Entity
 */
class UserWallet
{
    /**
     * @var \ArrayObject
     */
    private $transactions;

    public function __construct()
    {
        $this->transactions = new \ArrayObject();
    }

    /**
     * @return int
     */
    public function getBalance(): int
    {
        $sum = 0;
        foreach ($this->transactions as $transaction) {
            /** @var Transaction $transaction */
            if ($transaction->isDone()) {
                $sum += $transaction->getSum();
            }
        }

        return $sum;
    }

    public function addTransaction(Transaction $transaction)
    {
        $this->transactions->append($transaction);

        return $this;
    }
}