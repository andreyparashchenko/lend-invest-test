<?php

namespace LendInvest\Command;

/**
 * InterestCalculationCommand runs at the end of the month to calculate the interest of each investor to be paid.
 *
 * @package LendInvest\Command
 */
class InterestCalculationCommand implements CommandInterface
{
    public function execute()
    {
        // there should be code to transfer interest form payer to investor
    }
}