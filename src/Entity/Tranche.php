<?php

namespace LendInvest\Entity;

/**
 * Class Tranche
 * @package LendInvest\Entity
 */
class Tranche
{
    /**
     * @var string
     */
    private $code = '';

    /**
     * @var int
     */
    private $monthlyInterestPercentage = 0;

    /**
     * @var int
     */
    private $maxAmount = 0;

    /**
     * @var bool
     */
    private $done = false;

    /**
     * @var \ArrayObject
     */
    private $transactions;

    public function __construct()
    {
        $this->transactions = new \ArrayObject();
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getMonthlyInterestPercentage(): int
    {
        return $this->monthlyInterestPercentage;
    }

    /**
     * @param int $monthlyInterestPercentage
     */
    public function setMonthlyInterestPercentage(int $monthlyInterestPercentage)
    {
        $this->monthlyInterestPercentage = $monthlyInterestPercentage;
    }

    /**
     * @return int
     */
    public function getMaxAmount(): int
    {
        return $this->maxAmount;
    }

    /**
     * @param int $maxAmount
     */
    public function setMaxAmount(int $maxAmount)
    {
        $this->maxAmount = $maxAmount;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->done;
    }

    /**
     * @param bool $done
     */
    public function setDone(bool $done)
    {
        $this->done = $done;
    }

    public function getSumOfInvestment()
    {
        $sum = 0;
        foreach ($this->transactions as $transaction) {
            /** @var Transaction $transaction */
            if ($transaction->isDone()) {
                $sum += $transaction->getSum();
            }
        }

        return $sum;
    }

    public function addTransaction(Transaction $transaction)
    {
        $this->transactions->append($transaction);

        if ($transaction->isDone() && $this->maxAmount <= $this->getSumOfInvestment()) {
            // close tranche
            $this->done = true;
        }

        return $this;
    }

    /**
     * @return \ArrayObject
     */
    public function getTransactions()
    {
        return $this->transactions;
    }
}