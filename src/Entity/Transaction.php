<?php
/**
 * Created by PhpStorm.
 * User: paan
 * Date: 3/5/18
 * Time: 11:51 AM
 */

namespace LendInvest\Entity;

use LendInvest\Enhance\DateTime;

class Transaction
{
    const STATUS_NEW = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_DONE = 2;
    const STATUS_FAILED = 3;

    /**
     * @var DateTime
     */
    private $createdAt;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $sum = 0;

    /**
     * @var int
     */
    private $status = 0;

    /**
     * Transaction constructor.
     *
     * @param User $user
     * @param int  $sum
     * @param int  $status
     */
    public function __construct(User $user, int $sum, int $status = 0)
    {
        $this->createdAt = new DateTime();
        $this->user = $user;
        $this->sum  = $sum;
        $this->status = $status;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     */
    public function setCreatedAt(DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getSum(): int
    {
        return $this->sum;
    }

    /**
     * @param int $sum
     */
    public function setSum(int $sum)
    {
        $this->sum = $sum;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }

    public function isDone()
    {
        return self::STATUS_DONE === $this->status;
    }
}