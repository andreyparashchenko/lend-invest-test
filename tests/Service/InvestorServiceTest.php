<?php

namespace Tests;

use LendInvest\Enhance\DateTime;
use LendInvest\Entity\Investor;
use LendInvest\Entity\Loan;
use LendInvest\Entity\Tranche;
use LendInvest\Entity\Transaction;
use LendInvest\Service\InvestorService;
use PHPUnit\Framework\TestCase;

/**
 * Class InvestorServiceTest
 * @package Tests
 */
class InvestorServiceTest extends TestCase
{

    /**
     * @var InvestorService
     */
    private $investorService;

    /**
     * @var Loan
     */
    private $loan;

    public function setUp()
    {
        $this->investorService = new InvestorService();

        DateTime::shiftTime(null);

        $tranche1 = new Tranche();
        $tranche1->setCode('A');
        $tranche1->setMaxAmount(1000);
        $tranche1->setMonthlyInterestPercentage(3);

        $tranche2 = new Tranche();
        $tranche2->setCode('B');
        $tranche2->setMaxAmount(1000);
        $tranche2->setMonthlyInterestPercentage(6);

        $loanDateStart = new DateTime('2015-10-01');
        $loanDateEnd   = new DateTime('2015-11-15');
        $this->loan = new Loan($loanDateStart, $loanDateEnd);
        $this->loan->addTranche($tranche1);
        $this->loan->addTranche($tranche2);
    }

    /**
     * @expectedException LendInvest\Exception\LoanClosedForInvestmentException
     */
    public function testDoInvestmentLoanClosed()
    {
        DateTime::shiftTime(strtotime('2015-11-16'));
        $investor = new Investor();

        $this->investorService->doInvestment($investor, $this->loan, 'A', 100);
    }

    /**
     * @expectedException LendInvest\Exception\NotFoundException
     */
    public function testDoInvestmentTrancheIsNotFound()
    {
        DateTime::shiftTime(strtotime('2015-09-20'));
        $investor = new Investor();

        $this->investorService->doInvestment($investor, $this->loan, 'C', 100);
    }

    /**
     * @expectedException LendInvest\Exception\InvestmentNotDoneException
     */
    public function testDoInvestmentTrancheIsDoneOrNoMoney()
    {
        DateTime::shiftTime(strtotime('2015-09-20'));
        $investor = new Investor();

        $this->investorService->doInvestment($investor, $this->loan, 'B', 100);
    }

    public function testDoInvestment()
    {
        DateTime::shiftTime(strtotime('2015-09-20'));
        $investor = new Investor();
        $investor->getWallet()->addTransaction(new Transaction($investor, 1000, Transaction::STATUS_DONE));

        $this->investorService->doInvestment($investor, $this->loan, 'A', 100);
        $tranche = $this->loan->getTrancheByKey('A');

        $this->assertEquals(100, $tranche->getSumOfInvestment());
        $this->assertEquals(900, $investor->getWallet()->getBalance());
    }
}
