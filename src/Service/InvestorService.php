<?php

namespace LendInvest\Service;

use LendInvest\Enhance\DateTime;
use LendInvest\Entity\Investor;
use LendInvest\Entity\Loan;
use LendInvest\Entity\Tranche;
use LendInvest\Entity\Transaction;
use LendInvest\Exception\InvestmentNotDoneException;
use LendInvest\Exception\LoanClosedForInvestmentException;
use LendInvest\Exception\NotFoundException;

/**
 * Class InvestorService
 * @package LendInvest\Service
 */
class InvestorService
{

    /**
     * @param Investor $investor
     * @param Loan     $loan
     * @param string   $trancheKey
     * @param int      $sum
     *
     * @throws InvestmentNotDoneException
     * @throws LoanClosedForInvestmentException
     * @throws NotFoundException
     */
    public function doInvestment(Investor $investor, Loan $loan, string $trancheKey, int $sum)
    {
        if (!$loan->isOpenedForInvestment()) {
            throw new LoanClosedForInvestmentException();
        }

        $tranche = $loan->getTrancheByKey($trancheKey);
        if (!$tranche) {
            throw new NotFoundException();
        }

        if (!$investor->hasSumToInvest($sum) || $tranche->isDone()) {
            throw new InvestmentNotDoneException();
        }

        $this->investToTranche($investor, $tranche, $sum);
    }

    /**
     * @param Investor $investor
     * @param Tranche  $tranche
     * @param int      $sum
     */
    private function investToTranche(Investor $investor, Tranche $tranche, int $sum)
    {
        $investor->getWallet()->addTransaction($this->createTransaction($investor, -1 * $sum));
        $tranche->addTransaction($this->createTransaction($investor, $sum));
    }

    /**
     * @param Investor $investor
     * @param integer  $sum
     *
     * @return Transaction
     */
    private function createTransaction(Investor $investor, int $sum)
    {
        return new Transaction($investor, $sum, Transaction::STATUS_DONE);
    }

    /**
     * @param Loan     $loan
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     */
    public function transferMonthlyInterest(Loan $loan, DateTime $dateStart, DateTime $dateEnd)
    {
        foreach ($loan->getTranches() as $tranche) {
            /** @var Tranche $tranche */

            foreach ($tranche->getTransactions() as $transaction) {
                /** @var Transaction $transaction */
                $investor = $transaction->getUser();

                $coef = $this->calculatePeriodCoefficient($dateStart, $dateEnd, $transaction);
                $sumOfInterest = $transaction->getSum() * $tranche->getMonthlyInterestPercentage() * $coef / 100;

                $investor->getWallet()->addTransaction(
                    new Transaction($investor, $sumOfInterest, Transaction::STATUS_DONE)
                );

                // there must be transaction to get money from payer
            }
        }
    }

    /**
     * @param DateTime    $dateStart
     * @param DateTime    $dateEnd
     * @param Transaction $transaction
     *
     * @return float
     */
    private function calculatePeriodCoefficient(DateTime $dateStart, DateTime $dateEnd, Transaction $transaction)
    {
        $numberOfDays = cal_days_in_month(CAL_GREGORIAN, $dateStart->format('m'), $dateStart->format('Y'));
        $daysToPay = $numberOfDays;

        $timeStart = $dateStart->getTimestamp();
        $timeEnd   = $dateEnd->getTimestamp();

        $timeTransactionCreatedAt = $transaction->getCreatedAt()->getTimestamp();

        if ($timeStart < $timeTransactionCreatedAt && $timeTransactionCreatedAt < $timeEnd) {
            $daysToPay = $numberOfDays - $transaction->getCreatedAt()->format('d') + 1;
        }

        if ($timeTransactionCreatedAt < $timeStart && $timeTransactionCreatedAt < $timeEnd) {
            $daysToPay = $numberOfDays - $dateStart->format('d');
        }

        return $daysToPay / $numberOfDays;
    }
}