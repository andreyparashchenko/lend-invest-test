<?php

namespace Tests;

use LendInvest\Entity\Transaction;
use LendInvest\Entity\User;
use PHPUnit\Framework\TestCase;

/**
 * Class UserWalletTest
 * @package Tests
 */
class UserWalletTest extends TestCase
{

    public function testGetBalance()
    {
        $user = new User();
        $transaction1 = new Transaction($user, 22, Transaction::STATUS_DONE);
        $transaction2 = new Transaction($user, 11, Transaction::STATUS_IN_PROGRESS);
        $transaction3 = new Transaction($user, 224, Transaction::STATUS_FAILED);
        $transaction4 = new Transaction($user, 999, Transaction::STATUS_DONE);

        $wallet = $user->getWallet();
        $wallet->addTransaction($transaction1);
        $wallet->addTransaction($transaction2);
        $wallet->addTransaction($transaction3);
        $wallet->addTransaction($transaction4);

        $this->assertEquals(1021, $wallet->getBalance());
    }
}
