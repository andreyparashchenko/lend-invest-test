<?php

namespace LendInvest\Entity;

use LendInvest\Enhance\DateTime;

/**
 * Class Loan
 * @package LendInvest\Entity
 */
class Loan
{
    /**
     * @var \DateTime
     */
    private $dateStart;

    /**
     * @var \DateTime
     */
    private $dateEnd;

    /**
     * @var \ArrayObject
     */
    private $tranches;

    /**
     * Loan constructor.
     *
     * @param \DateTime $dateStart
     * @param \DateTime $dateEnd
     */
    public function __construct(\DateTime $dateStart, \DateTime $dateEnd)
    {
        $this->tranches  = new \ArrayObject();
        $this->dateStart = $dateStart;
        $this->dateEnd   = $dateEnd;
    }

    /**
     * @return \DateTime
     */
    public function getDateStart(): \DateTime
    {
        return $this->dateStart;
    }

    /**
     * @param \DateTime $dateStart
     */
    public function setDateStart(\DateTime $dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return \DateTime
     */
    public function getDateEnd(): \DateTime
    {
        return $this->dateEnd;
    }

    /**
     * @param \DateTime $dateEnd
     */
    public function setDateEnd(\DateTime $dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return \ArrayObject
     */
    public function getTranches(): \ArrayObject
    {
        return $this->tranches;
    }

    /**
     * @param Tranche $tranche
     *
     * @return $this
     */
    public function addTranche(Tranche $tranche)
    {
        $this->tranches->append($tranche);

        return $this;
    }

    /**
     * @param string $key
     *
     * @return Tranche|null
     */
    public function getTrancheByKey(string $key)
    {
        $result = null;
        foreach ($this->tranches as $tranche) {
            /** @var Tranche $tranche */
            if ($tranche->getCode() === $key) {
                $result = $tranche;
                break;
            }
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function isOpenedForInvestment()
    {
        $now = new DateTime();

        $isDatePeriodOver = $now->getTimestamp() > $this->dateEnd->getTimestamp();

        $hasOpenTranche = false;
        foreach ($this->tranches as $tranche) {
            /** @var Tranche $tranche */
            if (!$tranche->isDone()) {
                $hasOpenTranche = true;
            }
        }

        return !$isDatePeriodOver && $hasOpenTranche;
    }
}